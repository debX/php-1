<?php 
    require_once('Animal.php');
    require_once('frog.php');
    require_once('Ape.php');

    $sheep = new Animal("shaun");
    echo "Name : " . $sheep->name;
    echo "<br>";
    echo "Legs : " . $sheep->legs;
    echo "<br>";
    echo "Cold Blooded : " . $sheep->cold_blooded;
    echo "<br> <br>";

    $kodok = new Frog("buduk");
    echo "Name : " . $kodok->name;
    echo "<br>";
    echo "Legs : " . $kodok->legs;
    echo "<br>";
    echo "Cold Blooded : " . $kodok->cold_blooded;
    echo "<br>";
    echo $kodok->jump();
    echo "<br> <br>";

    $sungokong = new Ape("Kera Sakti");
    echo "Name : " . $sungokong->name;
    echo "<br>";
    echo "Legs : " . $sungokong->legs;
    echo "<br>";
    echo "Cold Blooded : " . $sungokong->cold_blooded;
    echo "<br>";
    echo $sungokong->yell();
?>